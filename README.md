# Supernote A5X/A6X Templates

A collection of templates posted from around the internet for the [Supernote][sn]
A5X and A6X.

[sn]: https://supernote.com/

View the rendered site at one of these locations:
* https://kitlaan.gitlab.io/supernote-templates/
* https://supernote-templates.mostlyuseful.tech/

# Creating Templates

The template format is PNG, 1404x1872 pixels, 72-dpi.

To install a template, copy the PNG file to the `MyStyle` directory.

If you are having problems having the template show up, make sure there are no
"special characters" or spaces in the filename.

## Colors

As a grayscale device, you can use [different shades of grey][color] in your template.
Note that the contrast setting of the device does affect how these shades are shown.

[color]: https://www.reddit.com/r/Supernote/comments/lmo94r/possible_color_values_for_templates/

Values seem to be from:
* darkest constrast setting: `#FFFFFF` to `#BBBBBB`.
* middle constrast setting: `#F6F6F6` to `#B0B0B0`.
* lightest constrast setting: `#EDEDED` to `#666666`.

Emperically, it seems that four common visible values are: `#FFFFFF`, `#E3E3E3`, `#CFCFCF`, and `#000000`.

# See Also

* https://rm.ezb.io/ -- Remarkable templates use the same dimensions, so they are directly usable.
