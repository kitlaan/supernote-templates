Sourced from [u/CountrywideToe @ reddit][reddit].

> The idea is that you can set this as the background of a note so that
> you can get a sense of what different opacities and line weights look
> like. I've also included a handful of font samples that I use frequently.

[reddit]: https://www.reddit.com/r/Supernote/comments/13fyj9w/for_those_of_you_designing_templates_for_the/

