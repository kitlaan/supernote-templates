#!/usr/bin/env python3

import os
import sys
import configparser
import json
import datetime
import praw

# [DEFAULT]
# ClientId = ...
# ClientSecret = ...
config = configparser.ConfigParser()
config.read('makejson.ini')

reddit = praw.Reddit(client_id=config['DEFAULT']['ClientId'],
                     client_secret=config['DEFAULT']['ClientSecret'],
                     user_agent="Metadata Puller")
reddit.read_only = True


entry = None
timestamp = None
author = None

if len(sys.argv) > 1:
    url = sys.argv[1]
else:
    url = input("Enter URL: ")
    url = url.split('?')[0]

if timestamp is None:
    try:
        entry = reddit.comment(url=url)
        timestamp = entry.created_utc
        author = entry.author
    except:
        pass
if timestamp is None:
    try:
        entry = reddit.submission(url=url)
        timestamp = entry.created_utc
        author = entry.author
    except:
        pass
if timestamp is None:
    print("Error retrieving URL")
    sys.exit(1)

ts = datetime.datetime.utcfromtimestamp(timestamp)

post_time = ts.strftime("%Y-%m-%d %H:%M:%S")
file_time = ts.strftime("%Y%m%d%H%M%S")

print(author)
print(post_time)
print()

payload = []

class CompactJSONEncoder(json.JSONEncoder):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.indentation_level = 0
        self.noformat = False

    def encode(self, obj):
        if isinstance(obj, (list, tuple)):
            if not obj:
                return "[ ]"
            elif self.noformat:
                output = [ self.encode(el) for el in obj ]
                return "[ " + ", ".join(output) + " ]"
            else:
                self.indentation_level += 1
                output = [ self.indent_str + self.encode(el) for el in obj ]
                self.indentation_level -= 1
                return "[\n" + ",\n".join(output) + "\n" + self.indent_str + "]"
        elif isinstance(obj, dict):
            if obj:
                self.noformat = True
                self.indentation_level += 1
                output = []
                for k, v in obj.items():
                    if len(output) == 0:
                        output.append(f"{json.dumps(k)}: {self.encode(v)}")
                    else:
                        output.append(self.indent_str + f"{json.dumps(k)}: {self.encode(v)}")
                self.indentation_level -= 1
                self.noformat = False
                return "{ " + ",\n".join(output) + "\n" + self.indent_str + "}"
            else:
                return "{ }"
        else:
           return json.dumps(obj)

    @property
    def indent_str(self) -> str:
        return ' ' * (self.indentation_level * self.indent)

# Loop and get the filename entries
allfiles = []

while True:
    print()

    filenames_raw = input("Files (comma separated): ")
    if not filenames_raw:
        break
    filenames = [file_time + "_" + filename.strip() + ".png" for filename in filenames_raw.split(',')]
    allfiles.extend(filenames)

    title = input("Title: ")

    #description = input("Description: ")
    description = ""

    tags_raw = input("Tags (comma separated): ")
    if tags_raw:
        tags = [x.strip() for x in tags_raw.split(',')]
    else:
        tags = []

    author_name = "u/" + author.name if author else '?'

    payload.append({
        "timestamp": post_time,
        "originURL": url,
        "filename": filenames,
        "tags": tags,
        "owner": author_name,
        "title": title,
        "description": description,
    })

if len(allfiles) == 0:
    sys.exit(0)

# Create the file
outfile = os.path.join("templates", file_time + ".json")
f = open(outfile, "w")
f.write(json.dumps(payload, indent=2, cls=CompactJSONEncoder))
f.close()

print()
print(outfile)
for file in allfiles:
    print(file)
